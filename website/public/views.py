from django.contrib.auth.models import User
from django.http.response import HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse
# from .forms import *
from .models import Category, Product
from django.contrib.auth.models import User
from django.utils import timezone
from django.contrib.auth.decorators import login_required, permission_required

# Create your views here.

def index(request):
    return render(request,"pages/home.html", {
        'categories' : Category.objects.all()
    })

def about(request):
    return render(request,"pages/about.html", {
        'categories' : Category.objects.all()
    })

def search(request):
    return render(request,"pages/search.html", {
        'categories' : Category.objects.all()
    })

def product(request):
    return render(request,"pages/product/product.html", {
        'categories' : Category.objects.all()
    })

def productCreate(request):
    return render(request,"pages/product/product-create.html", {
        'categories' : Category.objects.all()
    })

def cart(request):
    return render(request,"pages/cart.html", {
        'categories' : Category.objects.all()
    })

def register(request):
    return render(request,"registration/register.html", {
        'categories' : Category.objects.all()
    })

