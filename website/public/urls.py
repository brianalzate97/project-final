from django.urls import path
from . import views

app_name = "public"
urlpatterns = [
    path('', views.index, name="index"),
    path('acerca-de', views.about, name="about"),
    path('search', views.search, name="search"),
    path('carro', views.cart, name="cart"),
    path('producto', views.product, name="product"),
    path('crear-producto', views.productCreate, name="product-create"),
    path('accounts/register', views.register, name="register"),
]