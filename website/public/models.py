from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Category(models.Model):
	name = models.CharField(max_length=64,null=False)

	def __str__(self):
		return f"{self.name}"


class Product(models.Model):
	name = models.CharField(max_length=255,null=False)
	category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="clasification_category")
	price = models.IntegerField(null=True)
	quantity = models.IntegerField(null=True)
	image = models.FileField(upload_to='img/products/')
	description = models.TextField(null=True)

	def __str__(self):
		return f"{self.name} {self.category} {self.price} {self.quantity} {self.description}"

