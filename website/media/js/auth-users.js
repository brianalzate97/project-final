// forms

var formAccount = document.getElementById('form_register');
var formLogin = document.getElementById('form_login');
var formForgot = document.getElementById('form_forgot');


// form Login
formLogin?.addEventListener("submit", function(e) {

  e.preventDefault();

   var dataForm = new FormData(this); 
   var data = {
      '_token': dataForm.get('_token'),
      'email': dataForm.get('email'),
      'password': dataForm.get('password')
   };

   if(validateFormLogin('login'))
   {
       this.submit();   
   }
});


// form forgot
formForgot?.addEventListener("submit", function(e) {

  e.preventDefault();

   var dataForm = new FormData(this); 
   var data = {
      '_token': dataForm.get('_token'),
      'email': dataForm.get('email')
   };

   if(validateFormForgot('forgot'))
   {
       return send({data: data},'forgot');
   }
});


// form account
formAccount?.addEventListener("submit", function(e) {

  e.preventDefault();

   var dataForm = new FormData(this); 
   var data = {
      '_token': dataForm.get('_token'),
      'first_name': dataForm.get('first_name'),
      'last_name': dataForm.get('last_name'),
      'email': dataForm.get('email'),
      'username': dataForm.get('username'),
      'password': dataForm.get('password'),
      'password_confirm': dataForm.get('password_confirm'),
      'politicies': dataForm.get('politicies')
   };

   if(validateForm('register'))
   {
       if(btnCheck(data.politicies,'register'))
       {
         return this.submit();   
         // return createAccount({data: data},'register');
       }   
   }
});
 
// function validate form login

function validateFormLogin(form)
{
    var username = document.getElementById('username'+'_'+form);
    var password = document.getElementById('password'+'_'+form);

       
    if(username.value.trim() == '')
    {           
        message(textMessage.username,'danger',form);
        username.focus();

        return false;
    }
    else
    {
        closeMessage(form);
    }            

    if(password.value.trim() == '')
    { 
       message(textMessage.password,'danger',form);
       password.focus();

       return false;
    }
    else
    {
       closeMessage(form);
    }

    return true;
}

// function validate form forgot

function validateFormForgot(form)
{
    var email = document.getElementById('email'+'_'+form);
    var emailPer = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
          
    if(email.value.trim() == '')
    {           
        message(textMessage.email,'danger',form);
        email.focus();

        return false;
    }
    else
    {
        closeMessage(form);
    }            

    if(!emailPer.test(email.value.trim()))
    {
        message(textMessage.email_valid,'danger',form);
        email.focus();

        return false;
    }
    else
    {
        closeMessage(form);
    }

    return true;
}

// function validate form

function validateForm(form)
{
    var name = document.getElementById('name_'+form);
    var lastname = document.getElementById('last_name_'+form);
    var email = document.getElementById('email_'+form);
    var emailPer = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    var username = document.getElementById('username_'+form);
    var password = document.getElementById('password_'+form);
    var passwordConfirm = document.getElementById('password_confirm_'+form);

    if(name.value.trim() == '')
    {
        message(textMessage.name,'danger',form);
        name.focus();

        return false;
    }
    else
    {
        closeMessage(form);
    }

    if(lastname.value.trim() == '')
    {
        message(textMessage.last_name,'danger',form);
        lastname.focus();

        return false;
    }
    else
    {
        closeMessage(form);
    }

    if(username.value.trim() == '')
    {
        message(textMessage.username,'danger',form);
        username.focus();

        return false;
    }
    else
    {
        closeMessage(form);
    }
       
    if(email.value.trim() == '')
    {           
        message(textMessage.email,'danger',form);
        email.focus();

        return false;
    }
    else
    {
        closeMessage(form);
    }            


    if(!emailPer.test(email.value.trim()))
    {
        message(textMessage.email_valid,'danger',form);
        email.focus();

        return false;
    }
    else
    {
        closeMessage(form);
    }

    if(password.value.trim() == '')
    { 
       message(textMessage.password,'danger',form);
       password.focus();

       return false;
    }
    else
    {
       closeMessage(form);
    }

    if(passwordConfirm.value.trim() == '')
    { 
       message(textMessage.password_confirm,'danger',form);
       passwordConfirm.focus();

       return false;
    }
    else
    {
       closeMessage(form);
    }

    if(password.value !== passwordConfirm.value)
    { 
       message(textMessage.no_password_correct,'danger',form);
       passwordConfirm.focus();

       return false;
    }
    else
    {
       closeMessage(form);
    }

    return true;
}

// validate privacy policies

function btnCheck(data,form)
{
    var check = data; 

    if(check !== 'on')
    {      
     message(textMessage.politicies,'danger',form);       
      
     return false;
    }
    else
    {
     closeMessage(form);
    }

    return true;
}

// send data form

function send(data,form)
{
    var dataParams = data.data;
    var formC = document.getElementById('form_'+form);

    message(textMessage.send + '...','info',form);

    fetch(urlForgot,
    {
        method: 'POST',
        body: JSON.stringify({data:dataParams}),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
    })
    .then(async res => await res.json())
    .then(data => {

        message(data.message,data.type,form);
        formC.reset();
    });
}

// create account

function createAccount(data,form)
{
    var dataParams = data.data;
    var formC = document.getElementById('form_'+form);

    message(textMessage.validate + '...','info',form);
    btnSendCheck('active','politicies_'+form,'site');

    fetch(urlCreateAccount,
    {
        method: 'POST',
        body: JSON.stringify({data:dataParams}),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
    })
    .then(async res => await res.json())
    .then(data => {

        message(data.message,data.type,form);
        btnSendCheck('none','politicies_'+form,'site');

        if(data.status)
        {
            formC.reset();
            message(textMessage.login_message,'success',form);
            setTimeout(function(){
               return window.location = (data.url == '' ? urlLogin : data.url);
            }, 3000);
        }
    });
}

// message

function message(message,type,form)
{
   let view = `<div class="alert alert-dismissible alert-${type}">
            <button type="button" class="close" onclick="javascript:closeMessage('${form}');" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            ${message}
            </div>`;

   $('#alertMessage_'+form).show();
   $('#alertMessage_'+form).html(view);
}

// close message

function closeMessage(form)
{
    $('#alertMessage_'+form).hide();
    $('#alertMessage_'+form).html('');
}
  

// Disable button

function btnSendCheck(status,form,id)
{
  var btns = document.querySelectorAll('[data-btn-send]');
  
  btns.forEach((item) => {

    if(item.dataset.btnSend == form)
    {
      if(status === 'none')
      {
        item.removeAttribute("disabled");
        item.style = 'cursor:pointer';
      } 
      else 
      {
        item.setAttribute("disabled", true);
        item.style = 'cursor: not-allowed; box-shadow: none; opacity: 0.65;';

        var element = document.getElementById('form_'+id);
        element.scrollIntoView();
      }
    }
  });      
}


// show password 
function showPassword(input,button)
{
    var password = document.getElementById(input);
    var button = document.getElementById(button);
    var status = button.dataset.statusPassword;

    if(status === 'false')
    {
       password.setAttribute('type','text');
       button.dataset.statusPassword = true;
       button.classList.add('password-active');
    }
    else
    {
       password.setAttribute('type','password');
       button.dataset.statusPassword = false;
       button.classList.remove('password-active');
    }
}

function enablebtn(id,data)
{
    var btns = document.querySelectorAll('[data-btn-send]');
    var checkbox = data;

    btns.forEach((item) => {

        if(item.dataset.btnSend === id)
        {
          if(checkbox.checked)
          {
            item.removeAttribute("disabled");
            item.style = 'cursor:pointer';
          } 
          else 
          {
            item.setAttribute("disabled", true);
            item.style = 'cursor: not-allowed; box-shadow: none; opacity: 0.65;';
          }
        }
    });      
}