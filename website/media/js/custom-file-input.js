/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/

'use strict';

(function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{	

			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

      if(fileName == '')
      {
        inserImage();
      }

			// if( fileName )
			// 	label.querySelector( 'span' ).innerHTML = fileName;
			// else
			// 	label.innerHTML = labelVal;
		});

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));

  var maxSize = 4000000;
  // var maxSize = 10;


function archivos(evt) {
  var files = evt.target.files; // FileList object

  // Obtenemos la imagen del campo "file".
  for (var i = 0, f; f = files[i]; i++) {
    //Solo admitimos imágenes.
    if (!f.type.match('image.*')) {
        continue;
    }

    if(files[0].size > maxSize){
    	$('#imageAlert').fadeIn(500);
    	$('#messageAlert').html('La imagen ha excedido el tamaño permitido');
      $('#imageProduct').val('');
    	setTimeout(function(){
    		$('#imageAlert').fadeOut();
    	}, 5000);
    	return false;
    }

    var reader = new FileReader();

    reader.onload = (function(theFile) {
        return function(e) {
         // Insertamos la imagen
         inserImage(e.target.result);
         
         
        };
    })(f);

    reader.readAsDataURL(f);
  }
}

function inserImage(image = null)
{
  let img = baseRoot + 'img/add-image.png';

  if(image != null)
  {
    img = image;
  }

  // Insertamos la imagen
  $('#img_file').attr('style','background-image: url('+ img +');');
  document.getElementById('img_file').innerHTML = ['<img class="thumb" src="'+baseRoot+'img/backgroundCaja.png" style="width:100%;height: auto;"/>'].join('');
}
									
																											      
document.getElementById('imageProduct').addEventListener('change', archivos, false);


$('[data-remove-alert]').click(function(){

  let root = this;
  let alertContainer = root.parentElement;

  alertContainer.style.display = 'none';

});