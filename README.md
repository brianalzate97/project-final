# Proyecto Final

## Configuración de compilación

```bash
# carpeta sitio web
$ cd website
# serve with hot reload at 127.0.0.1:8000
$ python manage.py runserver

```

### Credenciales Super Admin
```bash
# ruta super admin - 127.0.0.1:8000/admin
user : brianalzate97
password : brian&123
```

### Nota
_No logré terminar el proyecto en su totalidad, de hecho he estado profundizando en django para hacer el proyecto pero no he tenido mucho éxito, sin embargo la maqueta esta completa y parte de la programación del sitio en django. Sé que no cumplí con la meta y al no terminar el proyecto es probable que pueda certificarme, sin embargo me gustaría que le des un vistazo al proyecto. Muchas gracias por el curso y por los conocimientos obtenidos._