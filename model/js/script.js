/*-------------------------------------------------------
                      Start Script
-------------------------------------------------------*/

var contentID = document.getElementById('contentID');
var cartBtn = document.getElementById('cartBtn');
var cartList = document.getElementById('cartList');

cartBtn?.addEventListener('click', function(e) {

    if(cartList.classList.contains('show'))
    {
        cartList.classList.remove('show');
    
        setTimeout(function(){
         cartList.removeAttribute('style');
        }, 500);
    }
    else
    {
        cartList.style.display = 'block';
    
        setTimeout(function(){
         cartList.classList.add('show');
        }, 30);
    }

    return eventsButtonsMenu();
});


var searchBtn = document.getElementById('searchBtn');
var searchContent = document.getElementById('searchContent');

searchBtn?.addEventListener('click', function(e) {

    if(searchContent.classList.contains('show'))
    {
        searchContent.classList.remove('show');
    
        setTimeout(function(){
         searchContent.removeAttribute('style');
        }, 500);
    }
    else
    {
        searchContent.style.display = 'block';
    
        setTimeout(function(){
         searchContent.classList.add('show');
        }, 30);
    }

    return eventsButtonsMenu();
});
/*-------------------------------------------------------
                events click
-------------------------------------------------------*/

// capture events windows click
function eventsButtonsMenu()
{
   window.addEventListener('click', function(e) {
      return actionEvents(event);
    });
} 

function actionEvents(event)
{
  if(!event.target.matches('#cartBtn')
    && !event.target.matches('#cartBtn > i')
    && !event.target.matches('#cartBtn > span')
    && !event.target.matches('#cartList')
    && !event.target.matches('#cartList > ul')
    && !event.target.matches('#cartList > ul > li')
    && !event.target.matches('#cartList > ul > li > .ct-ls-img-product')
    && !event.target.matches('#cartList > ul > li > .ct-ls-img-product > img')
    && !event.target.matches('#cartList > ul > li > .ct-ls-description')
    && !event.target.matches('#cartList > ul > li > .ct-ls-description > a')
    && !event.target.matches('#cartList > ul > li > .ct-ls-description > a > i')
    && !event.target.matches('#cartList > ul > li > .ct-ls-description > p')
    && !event.target.matches('#cartList > ul > li > .ct-ls-description > span')
    && !event.target.matches('#cartList > .ct-btns')
    && !event.target.matches('#cartList > .ct-btns > a')) 
  {
      var dropdowns = document.getElementById("cartList");
   
      if(dropdowns.classList.contains('show')) 
      {
        dropdowns.classList.remove('show');
        setTimeout(function(){
         dropdowns.removeAttribute('style');
        }, 500);
      }
  }

  if(!event.target.matches('#searchBtn')
    && !event.target.matches('#searchBtn > i')
    && !event.target.matches('#searchContent')
    && !event.target.matches('#searchContent > form')
    && !event.target.matches('#searchContent > form > input')
    && !event.target.matches('#searchContent > form > button')) 
  {
      var dropdowns = document.getElementById("searchContent");
   
      if(dropdowns.classList.contains('show')) 
      {
        dropdowns.classList.remove('show');
        setTimeout(function(){
         dropdowns.removeAttribute('style');
        }, 500);
      }
  }
}


/*-------------------------------------------------------
                     scroll
-------------------------------------------------------*/

$(window).scroll(function(){

    let header = document.getElementById('header');
    let topMenu = document.getElementById('topMenu');
    let logo = document.getElementById('logoMenu');
    let navbar = document.getElementById('navbar');

    if($(this).scrollTop() > 50)
    {           
        topMenu.classList.add('scroll');
        logo.setAttribute('width',65);
        logo.setAttribute('height','auto');
        navbar.style.boxShadow = '1px 1px 10px rgba(0,0,0,0.2)';
    } 
    else
    {
        topMenu.classList.remove('scroll');
        logo.setAttribute('width',100);
        logo.setAttribute('height','auto');
        navbar.removeAttribute('style');
    }
    
});

/*-------------------------------------------------------
                       Validate quantity
-------------------------------------------------------*/

// quantity product
$('[data-btn-quantity]').click(function(e){
    e.preventDefault();

    var array = JSON.stringify(eval("(" + this.dataset.btnQuantity + ")"));
    var data = JSON.parse(array);
    var quantityProduct = 0;

    var id = document.getElementById(data[0].id);
    var calc = 0;

    if(data[0].validQuanty !== undefined)
    {
      quantityProduct = parseInt(id.dataset.quantityProduct);
    }

    if(data[0].type == 'plus')
    {
      calc = (Number.isInteger(parseInt(id.value)) ? parseInt(id.value) : 0) + 1;
    }
    else
    {
      if(parseInt(id.value) > 1)
      {
        calc = parseInt(id.value) - 1;
      }
      else
      {
        calc = 1;
      }
    }

    if(quantityProduct >= calc)
    {
      id.value = calc;
    }
    else
    {
      id.value = quantityProduct;
    }

    
});

// quantity products cart

function quantityBtn(data)
{
    var id = document.getElementById(data.id);
    var calc = 0;
    var dataInfo = data.data;

    if(data.type == 'plus')
    {
      calc = (Number.isInteger(parseInt(id.value)) ? parseInt(id.value) : 0) + 1;
    }
    else
    {
      if(parseInt(id.value) > 1)
      {
        calc = parseInt(id.value) - 1;
      }
      else
      {
        calc = 1;
      }
      
    }

    id.value = calc;

   return updateProduct(dataInfo.product,parseInt(calc),dataInfo.combination,true);
}

// valide quantity product cart

function quantityProduct(event, input, data)
{
  var cont = document.getElementById(input);

  if(event.which == 13 || event.keyCode == 13)
  {
    if(cont.value > 0 && cont.value != '')
    {
      return updateProduct(data.product,parseInt(cont.value),data.combination,true);
    }
    else
    {
      remove_notify();
      add_notify('error',textMessage.quantity_invalid);
    }
  }
}

/*-------------------------------------------------------
                        Notications
-------------------------------------------------------*/

// notify
function add_notify(type, message)
{
  // type : error, info, success
  
  var text = '';
  var title = textMessage.notify;
 
  text = message + '\r\n';

  $(document).ready(function (){
        
        new PNotify({
            title: title,
            text: text,
            type: type,
            styling: 'bootstrap3'
        });
  });

}

function remove_notify()
{
   PNotify.removeAll();
}

function addMessage(message,type)
{
     view = `<div class="alert alert-dismissible alert-${type}">
              <button type="button" class="close" onclick="javascript:remove_Message();" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              ${message}
              </div>`;

     $('#alertMessage_cart').show();
     $('#alertMessage_cart').html(view);
}


function remove_Message()
{
  $('#alertMessage_cart').hide();
  $('#alertMessage_cart').html('');
}

/*-------------------------------------------------------
                just numbers or letters input
-------------------------------------------------------*/

// only numbers
function justNumberSite(e){
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
        return true;

        return /\d/.test(String.fromCharCode(keynum));
}

// only letters
function justLetterSite(e)
{
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false;

    for(var i in especiales) 
    {
        if(key == especiales[i])
        {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
    {
        return false;
    }
}

/*-------------------------------------------------------
                update attribute of message
-------------------------------------------------------*/

function setAttributeTxt(data,message)
{
   let response = message;

   for(let i = 0; i < data.length; i++)
   {
      if(message.trim() != '')
      {
         let rep = `:${data[i][0]}`;
         response = response.split(rep).join(data[i][1]);
      }
   }

   return response;
}

/*-------------------------------------------------------
                      End Script
-------------------------------------------------------*/